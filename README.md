# CI Pipeline with Jenkins

Create a CI Pipline with Jenkinsfile (Freestyle, Pipline, Multibranch Pipline)

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

CI Pipleline for a Java Maven application to build and push to the repository

*  Make Docker available on Jenkins server

* Install Build Tools (Maven,Node) in Jenkins 

* Create different Jenkins Job types (Freestyle, Pipline, Multibranch Pipline) for the Java Maven project with jenkinsfile to:

* Connect to the application's git repository 

* Build Jar 

* Build Docker Image 

* Push to private DockerHub repository 

## Technologies Used 

* Jenkins 

* Docker 

* Linux 

* Git

* Java 

* Maven 

* Nexus


## Steps 

Step 1: pull Docker image of jenkins with docker from nexus repo

     docker pull 142.93.37.150:8083/jenkins-with-docker:1.1

[Dockerfile used for Image](/images/01_dockerfile_content_for_docker_image.png)
[Jenkins with Docker image on nexus](/images/01_jenkins_image_with_docker_on_nexus.png)
[Pulling image from nexus](/images/02_pulling_image_from_nexus_repo_to_jenkins_server.png)

Step 2: Check if docker is installed in the running container by getting into the docker container 

     docker exec -it bffee7d874e1 /bin/bash 
     docker 

[Docker in Container](/images/03_docker_in_container.png)

Step 3: Install built tools either through Jenkins UI or directly in the jenkins container 

Jenkins UI:

[Tool Plugins on Jenkins](/images/04_installed_built_tools_plugins.png)

Installing tools Manually in Jenkins container:

     docker exec -u 0 -it bffee7d874e1 bash
     curl -sL https://deb.nodesource.com/setup_19.x -o nodesource_setup.sh
     nodesource_setup.sh
     apt install nodejs  
  
Step 4: Create Jenkins Credentials for git repository 

[Credentials](/images/05_jenkins_credentials.png)

Step 5: Start a freestyle job and in configuration set source code management to your git with credentials created in step 4.

Step 6: Insert necessary built steps with plugins downloaded from UI or interactive terminal if it was downloaded manually 

invoke top level maven targets:

     Maven test  
     Maven Package

execute shell:

     docker build -t 142.93.37.150:8083/java-maven-app:1.1 .
     echo $PASSWORD | docker login -u $USERNAME --password-stdin 142.93.37.150:8083
     docker push 142.93.37.150:8083/java-maven-app:1.1

[Freestyle credentials](/images/06_freestyle_job_credentials.png)
[Freestyle Job logs](/images/07_freestyle_jobs_logs.png)

This will push docker image to nexus repo in which i had created 
[Image on nexus](/images/07_pushed-to_nexus.png)
[My nexus Server](/images/07_servers.png)

Step 7: Create a pipline for the same job using Jenkinsfile and groovy script 

 - In Pipline configuration insert the correct URL and credentials for the repo you want the job to run

 - Insert script path in configuration as well which is the path of the Jenkins file 

 - Create a Jenkins file in git repo which has the Pipline logic in it 

 - You could also create a groovy script which is another file that will have Pipline job logic in it and call the functions in Jenkins file however you would have to have the init stage in your Jenkinsfile. This is to allow Jenkinsfile know where to read the called functions 

My Jenkinsfile & groovy script are stored in root directory of repo

[Pipline Job log](/images/08_pipline_jobs_console_output.png)

Step 8: Create a multibranch pipline for the same job using Jenkinsfile and groovy script

 - In Pipline configuration insert the correct URL and credentials for the repo you want the job to run

 - Insert script path in configuration  which is the path of the Jenkins file 

 - To enable it to perform jobs to all the branches make sure you enable filter by name regular expression .*

 - Create Jenkinsfile and groovy script on git repo 

 - To avoid other branches that arent the master or main branch from building application you need to put an expression in the build stage in Jenkinfile or groovy script. This expression will contain environmental variable in which Jenkins provides us called BRANCH_NAME. You will need to give it the master or main branch as a value BRANCH_NAME == master. This will enable logic to only build on master branch.  

[Branch Name](/images/09_branch_name.png)

My Jenkinsfile & groovy script are stored in root directory of repo

[Mbranch Pipline](/images/09_multi_branch_pipline.png)
[Docker Repo](/images/10_docker_repo.png)

## Installation

Run $ apt install nodejs 

## Usage 

Run $ java -jar java-maven-app-*.jar


## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/creating-ci-pipeline-with-jenkins.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using mvn test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/creating-ci-pipeline-with-jenkins

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.

